import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesS4Component } from './classes-s4.component';

describe('ClassesS4Component', () => {
  let component: ClassesS4Component;
  let fixture: ComponentFixture<ClassesS4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassesS4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesS4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
