import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrainerRoutingModule } from './trainer-routing.module';
import { TrainerComponent } from './trainer/trainer.component';
import { TrainerS1Component } from './trainer-s1/trainer-s1.component';
import { TrainerS2Component } from './trainer-s2/trainer-s2.component';


@NgModule({
  declarations: [
    TrainerComponent,
    TrainerS1Component,
    TrainerS2Component
  ],
  imports: [
    CommonModule,
    TrainerRoutingModule
  ]
})
export class TrainerModule { }
