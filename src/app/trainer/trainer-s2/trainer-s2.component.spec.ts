import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerS2Component } from './trainer-s2.component';

describe('TrainerS2Component', () => {
  let component: TrainerS2Component;
  let fixture: ComponentFixture<TrainerS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainerS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
