import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerS1Component } from './trainer-s1.component';

describe('TrainerS1Component', () => {
  let component: TrainerS1Component;
  let fixture: ComponentFixture<TrainerS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainerS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
